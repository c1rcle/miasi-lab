grammar Lab1;

plik
    : (logic {System.out.println("Result: " + $logic.out);} NL+)* EOF
    ;
    
logic returns [Integer out]
    : outer=shift {$out = $outer.out;} ((AND inner=shift {$out &= $inner.out;}
    | OR inner=shift {$out |= $inner.out;}))*
    ;

shift returns [Integer out]
    : outer=expr {$out = $outer.out;} ((SL inner=expr {$out <<= $inner.out;}
    | SR inner=expr {$out >>= $inner.out;}))*
    ;
    
expr returns [Integer out]
    : outer=term {$out = $outer.out;} ((PLUS inner=term {$out += $inner.out;}
    | MINUS inner=term {$out -= $inner.out;}))*
    ;
    
term returns [Integer out]
    : outer=atom {$out = $outer.out;} ((MUL inner=atom {$out *= $inner.out;}
    | DIV inner=atom {
        if ($inner.out == 0) throw new ArithmeticException();
        $out /= $inner.out;
        }
    | MOD inner=atom {$out \%= $inner.out;}))*
    ;
    
atom returns [Integer out]
    : INT {$out = Integer.parseInt($INT.text);}
    | LP logic RP {$out = $logic.out;}
    ;

ID
    : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT
    : '0'..'9'+
    ;

PLUS
    : '+'
    ;

MINUS
    : '-'
    ;

MUL
    : '*'
    ;

DIV
    : '/'
    ;
    
MOD
    : '%'
    ;
    
AND
    : '&'
    ;

OR
    : '|'
    ;

SL
    : '<'
    ;

SR
    : '>'
    ;

NL	
    : '\n'
    ;
    
LP
    : '('
    ;
    
RP
    : ')'
    ;

COMMENT
    : '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    | '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

WS  
    : ( ' '
    | '\t'
    | '\r') {$channel=HIDDEN;}
    ;
